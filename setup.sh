# Install Mosquitto MQTT Broker
sudo apt install -y mosquitto

# Start the Mosquitto service
sudo systemctl start mosquitto

# Enable Mosquitto to start at boot
sudo systemctl enable mosquitto

# Check the status of Mosquitto
sudo systemctl status mosquitto


sudo mosquitto_passwd -c /etc/mosquitto/passwd your_username

password_file /etc/mosquitto/passwd
allow_anonymous false

listener 1883

protocol mqtt
listener 9001

protocol websockets



